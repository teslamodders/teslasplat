﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeslaLib;
using UnityEngine;

namespace FallDamage
{
    public class FallDamageWatcher : MonoBehaviour
    {
        void Update()
        {
            if (Player.player != null && Player.player.GetComponent<FallDamageComponent>() == null)
            {
                Player.player.gameObject.AddComponent<FallDamageComponent>();
            }
        }

        void OnDestroy()
        {
            if (Player.player != null)
            {
                FallDamageComponent fallDamageComponent = Player.player.GetComponent<FallDamageComponent>();
                if (fallDamageComponent != null)
                {
                    Destroy(fallDamageComponent);
                }
            }

        }
    }
}
