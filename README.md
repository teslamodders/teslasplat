# Teslasplat
A Teslagrad stupid challenge !

## What is this ?

Teslagrad, but with **impact damage** ! Everytime Teslakid collides with anything, if the collision is too violent, he dies.

Every collision with a relative speed of 10+ Teslaspeed is displayed on screen, and if the relative speed is 15+, Teslakid dies.

## Why ?

The early prototype of Teslagrad had fall damage, so I was curious to see if the game was playable with it or not.

And because I have too much free time, probably.

## How to install
Disclamer first : I am just a Teslagrad fan, and **not affiliated in any way with Rain-Games**. 

Little warning : this mod have been mostly tested in the 1.3.1 version of Teslagrad, which is the version speedrunners use. It *may* also work with the latest version, but if it doesn't, download the 1.3.1 using steam betas (right click on your game > properties > betas > 1.3.1)

- Download the [latest release](https://gitlab.com/teslamodders/teslasplat/-/raw/main/builds/Teslasplat%201.0.0.zip)
- Go to your Teslagrad "Managed" folder (usually something like C:\SteamLibrary\steamapps\common\Teslagrad\Teslagrad_Data\Managed)
- Extract the mod files in that folder. It will ask you to replace Assembly-Boo.dll, say yes.
- Launch your game : a "Teslamods" button should have appeared in the top-right corner of your screen.

## How to play
- Enable the mod by checking "Teslasplat" ! You can also uncheck the box to disable the mod.
- If you are stuck, press the "I" key to activate a 5 seconds immunity to impacts

## How to uninstall

- Delete all the mods file.
- If you didn't keep a copy of Assembly-Boo.dll, use Steam Integrity check to revert the modifications (Right click on your game > Properties > Local Files > Check files integrity)

Maybe, one day, I'll create an easier installer.

Maybe.
