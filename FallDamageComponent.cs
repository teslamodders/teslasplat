﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeslaLib;
using UnityEngine;

namespace FallDamage
{
    public class FallDamageComponent : MonoBehaviour
    {
        private float speedLimit = 15f;
        private float invulnerabilityStartTime = 0f;
        private float invulnerabilityTime = 5f;
        private InGameText invulnerabilityText = new InGameText();

        void Start()
        {
            invulnerabilityText.SetColor(Color.yellow);
            invulnerabilityText.SetSize(150);
        }

        public void OnCollisionEnter(Collision collision)
        {
            float previousContactSpeed = 0f;
            foreach (ContactPoint p in collision.contacts)
            {
                float speed = Vector3.Scale(p.normal, collision.relativeVelocity).magnitude;
                if (speed > speedLimit / 1.5f && previousContactSpeed != speed)
                {
                    InGameText text = new InGameText();
                    text.SetColor(Color.white);
                    text.SetPosition(p.point);
                    text.SetText(speed.ToString());
                    text.AddEffect();
                    previousContactSpeed = speed;

                    if (speed >= speedLimit)
                    {
                        if (Time.time - invulnerabilityStartTime <= invulnerabilityTime)
                        {
                            text.SetColor(Color.yellow);
                        } else if (!Player.player.IsDying()) {
                            text.SetColor(Color.red);
                            Player.player.Die(CauseOfDeath.Faint);
                        }
                    }
                }
            }
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                invulnerabilityStartTime = Time.time;
            }

            float remainingInvulnerabilityTime = invulnerabilityTime - (Time.time - invulnerabilityStartTime);

            if (remainingInvulnerabilityTime > 0f)
            {
                invulnerabilityText.SetPosition(transform.position);
                invulnerabilityText.SetText("No impact damage for " + remainingInvulnerabilityTime.ToString("F") + " seconds");
            } else {
                invulnerabilityText.SetText("");
            }
        }
    }
}
