﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeslaMod;
using UnityEngine;

namespace FallDamage
{
    public class FallDamage : TeslaModInterface
    {
        public GameObject go;

        public string GetName()
        {
            return "Teslasplat";
        }

        public void OnDisable()
        {
            GameObject.Destroy(go);
        }

        public void OnEnable()
        {
            go = new GameObject();
            go.AddComponent<FallDamageWatcher>();
            GameObject.DontDestroyOnLoad(go);
        }

        public void OnReload()
        {
            
        }
    }
}
